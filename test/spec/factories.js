/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('TestModule', [ 'seen-test', 'seen-shop' ]);

describe('Factories ', function () {
    var $shop;
    var $test;

    beforeEach(function () {
        module('TestModule');
        inject(function (_$shop_, _$test_) {
            $shop = _$shop_;
            $test = _$test_;
        });
    });

    function testForResource(factory, resource) {
        it('should contains function of ' + resource.name, function () {
            $test.factoryCollectionFunctions(factory, resource);
        });
    }

    function testFor(factory) {
        // Function test
        it('should contains basic functions', function () {
            $test.factoryBasicFunctions(factory);
        });

        it('should call POST:' + factory.url + ' to update', function (done) {
            $test.factoryUpdateFunctions(factory, done);
        });
        it('should call DELETE:' + factory.url + ' to update', function (done) {
            $test.factoryDeleteFunctions(factory, done);
        });

        if (angular.isArray(factory.resources)) {
            for (var j = 0; j < factory.resources.length; j++) {
                testForResource(factory, factory.resources[j]);
            }
        }
    }

    var factories = [ {
        factory : 'ShopProduct',
        url : '/api/v2/shop/product',
        url: '/api/v2/shop/products',
        resources: [{
            name: 'Metafield',
            factory: 'ProductMetafield',
            type: 'collection',
            url: '/metafields'
        }, {
            name: 'Category',
            factory: 'ShopCategory',
            type: 'collection',
            url: '/categories'
        }, {
            name: 'Tag',
            factory: 'ShopTag',
            type: 'collection',
            url: '/tags'
        }, {
            name: 'Tax',
            factory: 'ShopTax',
            type: 'collection',
            url: '/taxes'
        }]
    }, {
        factory: 'ShopService',
        url: '/api/v2/shop/services',
        resources: [{
            name: 'Metafield',
            factory: 'ServiceMetafield',
            type: 'collection',
            url: '/metafields'
        }, {
            name: 'Category',
            factory: 'ShopCategory',
            type: 'collection',
            url: '/categories'
        }, {
            name: 'Tag',
            factory: 'ShopTag',
            type: 'collection',
            url: '/tags'
        }, {
            name: 'Tax',
            factory: 'ShopTax',
            type: 'collection',
            url: '/taxes'
        }]
    }, {
        factory : 'ProductMetafield',
        url : '/api/v2/shop/product-metafields'
    }, {
        factory : 'ServiceMetafield',
        url : '/api/v2/shop/service-metafields'
    }, {
        factory : 'ShopAddress',
        url : '/api/v2/shop/addresses'
    }, {
        factory : 'ShopContact',
        url : '/api/v2/shop/contacts'
    }, {
        factory : 'ShopAgency',
        url : '/api/v2/shop/agencies',
        resources : [ {
            name : 'Order',
            factory : 'ShopOrder',
            type : 'collection',
            url : '/orders'
        } ]
    }, {
        factory : 'ShopZone',
        url : '/api/v2/shop/zones',
        resources : [ {
            name : 'Member',
            factory : 'UserAccount',
            type : 'collection',
            url : '/members'
        }, {
            name : 'Order',
            factory : 'ShopOrder',
            type : 'collection',
            url : '/orders'
        } ]
    }, {
        factory : 'ShopOrder',
        url : '/api/v2/shop/orders',
        resources : [ {
            name : 'History',
            factory : '',
            type : 'collection',
            url : '/histories'
        }, {
            name : 'Item',
            factory : '',
            type : 'collection',
            url : '/items'
        } ]
    }, {
        factory : 'ShopOrder',
        url : '/api/v2/shop/orders',
        resources : [ {
            name : 'History',
            factory : '',
            type : 'collection',
            url : '/histories'
        }, {
            name : 'Item',
            factory : '',
            type : 'collection',
            url : '/items'
        } ]
    }, {
        factory : 'ShopItem',
        url : '/api/v2/shop/items',
        resources : [ {
            name : 'Category',
            factory : 'ShopCategory',
            type : 'collection',
            url : '/categories'
        }, {
            name : 'Tag',
            factory : 'ShopTag',
            type : 'collection',
            url : '/tags'
        }, {
            name : 'Meta',
            factory : 'ShopMeta',
            type : 'collection',
            url : '/metas'
        } ]
    }, {
        factory : 'ShopCategory',
        url : '/api/v2/shop/categories',
        resources : [ {
            name : 'Item',
            factory : '',
            type : 'collection',
            url : '/items'
        } ]
    }, {
        factory : 'ShopMeta',
        url : '/api/v2/shop/metas'
    }, {
        factory : 'ShopTag',
        url : '/api/v2/shop/tags',
        resources : [ {
            name : 'Item',
            factory : '',
            type : 'collection',
            url : '/items'
        } ]
    }, {
        factory : 'ShopTax',
        url : '/api/v2/shop/taxes',
        resources : [ {
            name : 'Item',
            factory : '',
            type : 'collection',
            url : '/items'
        } ]
    } ];
    for (var i = 0; i < factories.length; i++) {
        testFor(factories[i]);
    }
});
